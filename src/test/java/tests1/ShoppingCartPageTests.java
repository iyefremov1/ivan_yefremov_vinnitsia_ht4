package tests1;

import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ShoppingCartPageTests extends BaseTest {
    private final long TIME_TO_WAIT = 60;
    private final String SEARCH_CODE = "335388493";
    private final String MUST_BE_IN_CART = "2";

    @Test(invocationCount = 1)
    public void checkCartDisplayCorrectQuantityOfOrderedGoods() throws InterruptedException {

        getHomePage().elementToBeClickable(getHomePage().getSearchInputField(), TIME_TO_WAIT);
        getHomePage().getSearchInputField().sendKeys(SEARCH_CODE);
        getHomePage().getSearchInputField().sendKeys(Keys.ENTER);

        getSearchResultPage().visibilityOfElement(getSearchResultPage().getBuyGreenButton(), TIME_TO_WAIT);

         getDriver().navigate().refresh();
         getSearchResultPage().elementToBeClickable(getSearchResultPage().getBuyGreenButton(), TIME_TO_WAIT);
         getSearchResultPage().getBuyGreenButton().click();

         getShoppingCartPage().elementToBeClickable(getShoppingCartPage().getIncrementButton(), TIME_TO_WAIT);
         getShoppingCartPage().getIncrementButton().click();

         getShoppingCartPage().waitTillElementIsChanged(getDriver(), getHomePage().getShopCartIconBY(), "2",5);
         getShoppingCartPage().getContinueShoppingLink().click();

        Assert.assertEquals(getHomePage().getShopCartIcon().getText(), MUST_BE_IN_CART);
    }
}
