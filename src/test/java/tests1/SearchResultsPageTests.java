package tests1;

import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SearchResultsPageTests extends BaseTest {

    private final String SEARCH_CODE = "335388493";
    private final String MUST_BE_CODE = "335388493";
    private final long TIME_TO_WAIT = 30;

    @Test
    public void verifyUserCanSearchItemByCode() throws InterruptedException {

        getHomePage().visibilityOfElement(getHomePage().getSearchInputField(), TIME_TO_WAIT);
        getHomePage().getSearchInputField().clear();
        getHomePage().getSearchInputField().sendKeys(SEARCH_CODE);
        getHomePage().getSearchInputField().sendKeys(Keys.ENTER);

        getSearchResultPage().visibilityOfElement(getSearchResultPage().getCodeOfSearchGoods(), TIME_TO_WAIT);

        String str = getSearchResultPage().getCodeOfSearchGoods().getAttribute("innerText");

        Assert.assertTrue(str.contains(MUST_BE_CODE), "The code values are not equal");
    }
}
