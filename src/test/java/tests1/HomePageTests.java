package tests1;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class HomePageTests extends BaseTest{

    protected final long TIME_TO_WAIT = 30;
    List<String> elementsMustBeInMainContainer = Arrays.asList(
           "Ноутбуки та комп’ютери",
           "Смартфони, ТВ і електроніка",
            "Товари для геймерів",
            "Побутова техніка",
            "Товари для дому",
            "Інструменти та автотовари",
            "Сантехніка та ремонт",
            "Дача, сад і город",
            "Спорт і захоплення",
            "Одяг, взуття та прикраси",
            "Краса та здоров’я",
            "Дитячі товари",
            "Зоотовари",
            "Канцтовари та книги",
            "Алкогольні напої та продукти",
            "Товари для бізнесу та послуги",
            "Нове на весну до -45%"
    );

    @Test
    public void checkIfMainNavigationContainerHasAllItems() {
        getHomePage().waitMainContainerIsDisplayed(TIME_TO_WAIT);

        assertTrue(getHomePage().getMainCatalogElementsList().containsAll(elementsMustBeInMainContainer),
                "Incorrect elements for the main container");
    }
}
