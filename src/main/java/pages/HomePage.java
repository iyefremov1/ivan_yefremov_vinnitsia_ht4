package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.stream.Collectors;

public class HomePage extends BasePage {


    @FindBy(xpath = "//ul[contains(@class,'_main')]/li")
    private List<WebElement> mainContainerCategoryList;

    @FindBy(xpath = "//a[contains(@href,'ua/brands-list')]")
    private WebElement brandButton;

    @FindBy(xpath = "//rz-cart[contains(@class,'header')]//button[contains(@class,'header__button')]")
    private WebElement shopCartIcon;

    private static By shopCartIconBY = By.xpath("//rz-cart[contains(@class,'header')]//button[contains(@class,'header__button')]");

    @FindBy(xpath = "//input[contains(@class,'search-form__input')]")
    private WebElement searchInputField;

    @FindBy(xpath = "//a[contains(@class,' search-suggest__show-all')]")
    private WebElement showAllResult;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public By getShopCartIconBY() {
        return shopCartIconBY;
    }

    public List<WebElement> getMainContainerCategoryList() {
        return mainContainerCategoryList;
    }

    public void waitMainContainerIsDisplayed(long timeToWait) {
        visibilityOfElements(getMainContainerCategoryList(), timeToWait);
    }

    public List<String> getMainCatalogElementsList() {

        return getMainContainerCategoryList().stream().map(x -> x.getText()).collect(Collectors.toList());
    }

    public WebElement getBrandButton() {
        return brandButton;
    }

    public WebElement getShopCartIcon() {
        return shopCartIcon;
    }

    public WebElement getSearchInputField() {
        return searchInputField;
    }

    public WebElement getShowAllResult() {
        return showAllResult;
    }
}
