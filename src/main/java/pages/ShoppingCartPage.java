package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ShoppingCartPage extends BasePage {

    @FindBy(xpath = "//span[contains(@class,'close-modal')]")
    private WebElement continueToBuyButton;

    @FindBy(xpath = "//button[contains(@data-testid, 'increment-button')]")
    private WebElement incrementButton;

    @FindBy(xpath = "//button[contains(@data-testid,'continue-shopping-link')]")
    private WebElement continueShoppingLink;

    public ShoppingCartPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getContinueToBuyButton() {
        return continueToBuyButton;
    }

    public WebElement getIncrementButton() {
        return incrementButton;
    }

    public WebElement getContinueShoppingLink() {
        return continueShoppingLink;
    }
}
