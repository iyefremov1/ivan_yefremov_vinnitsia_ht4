package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class BasePage {
    protected WebDriver driver;

    BasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriverWait getWaiter(long seconds) {
        return new WebDriverWait(driver, seconds);
    }

    public void visibilityOfElement(WebElement element, long timeToWait) {
        WebDriverWait wait = new WebDriverWait(driver, timeToWait);
        wait.until(ExpectedConditions.not(ExpectedConditions.stalenessOf(element)));
        wait.until(ExpectedConditions.visibilityOf(element));

    }

    public void visibilityOfElements(List<WebElement> elements, long timeToWait) {
        WebDriverWait wait = new WebDriverWait(driver, timeToWait);
        wait.until(ExpectedConditions.visibilityOfAllElements(elements));

    }

    public void elementToBeClickable(WebElement element, long timeToWait) {
        WebDriverWait wait = new WebDriverWait(driver, timeToWait);
        wait.until(ExpectedConditions.not(ExpectedConditions.stalenessOf(element)));
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitTillElementIsChanged(WebDriver driver, By by, String waitValue, long seconds) {
        try {
            new WebDriverWait(driver, TimeUnit.SECONDS.toSeconds(seconds))
                    .until((ExpectedCondition<Boolean>) x -> driver
                            .findElement(by).getText().equals(waitValue));
        } catch(Exception e) {
            e.getCause();
        }
    }

    public void clickUsingJS(WebDriver driver, WebElement element) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
    }
}
