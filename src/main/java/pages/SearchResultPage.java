package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultPage extends BasePage {

    public SearchResultPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//p[contains(@class,'product__code detail-code')]")
    private WebElement codeOfSearchGoods;

    @FindBy(xpath = "//button[contains(@class,'button button--with-icon')]")
    private WebElement buyGreenButton;

    public WebElement getCodeOfSearchGoods() {
        return codeOfSearchGoods;
    }

    public WebElement getBuyGreenButton() {
        return buyGreenButton;
    }
}
